<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">
      <!-- Dashboard Nav -->
      <li class="nav-item {{ Request::is('dashboard') ? 'active' : '' }} ">
        <a class="nav-link " href="{{ url('dashboard') }}">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->
      <!-- Driver Nav -->
      <li class="nav-item">
        <a class="nav-link " href="index.html">
          <i class="bi bi-hammer"></i>
          <span>Driver List</span>
        </a>
      </li><!-- End Driver Nav -->
      <!-- Vehicle List Nav -->
      <li class="nav-item">
        <a class="nav-link " href="index.html">
          <i class="bi bi-truck"></i>
          <span>Vehicle List</span>
        </a>
      </li><!-- End Vehicle List Nav -->
      <!-- Vehicle Ownership Nav -->
      <li class="nav-item">
        <a class="nav-link " href="index.html">
          <i class="bi bi-people"></i>
          <span>Vehicle Ownership</span>
        </a>
      </li><!-- End Vehicle List Nav -->
      <!-- Rental Nav -->
      <li class="nav-item">
        <a class="nav-link " href="index.html">
          <i class="bi bi-calendar2"></i>
          <span>Rent List</span>
        </a>
      </li><!-- End Rental Data Nav -->
      <!-- Logout Nav -->
      <li class="nav-item">
        <a class="nav-link " href="index.html">
          <i class="bi bi-box-arrow-right"></i>
          <span>Logout</span>
        </a>
      </li><!-- End Logout Nav -->
    </ul>
  </aside>