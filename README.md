# sekawanmedia

### Credentials
##### Admin
        - Username : admin@mail.com
        - Password : password

##### Region Manager
        - Username : region_manager@mail.com
        - Password : passworod

##### Branch Manager
        - Username : branch_manager@mail.com
        - Password : password

### Requirement
- Laravel (composer)
- php 7.4+
- Node
- MySQL

### Installation 
    - git clone https://gitlab.com/Zaki140502/sekawanmedia.git
    - cd sekawanmedia
    - composer install
    - cp .env.example .env
    - php artisan key:generate
    - npm install
    - npm run dev
    - php artisan serve

### Migrating database
    - php artisan migrate
    - php artisan serve

### Seed Dummy Data
you can set the specifik data on database/seeders then run

    - php artisan db:seed

### Database Design 
    - https://drive.google.com/file/d/1oylq4YOGOsWBEcfyxMy4JP0Y8fmFPfN-/view?usp=sharing
