<?php

namespace App\Models;

use App\Models\Vehicle;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class VehicleOwnership extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_name',
        'address',
        'phone_number',
    ];

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }
}
